﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dal.Models;
using Services;
using FormModels = Dal.FormModels;

namespace Presentation.Books
{
    public partial class AddBook : Form
    {
        private readonly AuthorService _authorService;
        private readonly CategoryService _categoryService;
        private readonly BooksService _booksService;
        public AddBook()
        {
            InitializeComponent();
            _authorService = new AuthorService();
            _categoryService = new CategoryService();
            _booksService = new BooksService();
            ConfigureForm();
        }

        ~AddBook()
        {
            _authorService.Dispose();
            _categoryService.Dispose();
            _booksService.Dispose();
        }

        public void ConfigureForm()
        {
            listBoxCategories.DisplayMember = "Name";
            listBoxCategories.ValueMember = "Id";

            listBoxAuthors.DisplayMember = "FullName";
            listBoxAuthors.ValueMember = "Id";

            FillCategoryListbox();
            FillAuhorListbox();
        }

        public void FillCategoryListbox()
        {
            var categories = _categoryService.GetCategoriesNew();
            listBoxCategories.DataSource = new BindingSource(categories, null);
            listBoxCategories.SelectedItem = null;
        }

        public void FillAuhorListbox()
        {
            var authors = _authorService.GetAuthors();
            listBoxAuthors.DataSource = new BindingSource(authors, null);
            listBoxAuthors.SelectedItem = null;
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            var addBook = new FormModels.AddBook
            {
                Title = txtTitle.Text,
                DateOfPublish = dateDateOfPublish.Value,
                CategoryIds = new List<int>(),
                AuthorIds = new List<int>()
            };

            foreach (CategoryModel category in listBoxCategories.SelectedItems)
            {
                addBook.CategoryIds.Add(category.Id);
            }

            foreach (FormModels.Author author in listBoxAuthors.SelectedItems)
            {
                addBook.AuthorIds.Add(author.Id.Value);
            }

            _booksService.AddBook(addBook);
        }
    }
}
