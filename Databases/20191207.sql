CREATE DATABASE  IF NOT EXISTS `library` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `library`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: library
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `authors` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Firstname` varchar(150) DEFAULT NULL,
  `Lastname` varchar(250) DEFAULT NULL,
  `Nationality` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,'Dominykas','Sadauskas','Lietuva'),(2,'Laimonas','Lipinskas','Lietuva'),(3,'Kasparas','Čiurlionis','Lietuva'),(4,'Patrik','Voronovič','Lietuva'),(5,'Alan','Kolderas','Lietuva');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `books` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Isbn` varchar(150) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `DateOfPublish` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `INDX_ISBN` (`Isbn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1,'99921-58-10-7','criss-cross',NULL),(2,'9971-5-0210-0','The Family Upstairs',NULL),(3,'960-425-059-0','Watercolor School : A Practical Guide to Painting with Watercolor One Step at a Time',NULL),(4,'80-902734-1-6','Zoom (Picture Puffin Books)',NULL),(5,'85-359-0277-5','Who Was Leonardo da Vinci?',NULL),(6,'1-84356-028-3','Best of the Joy of Painting',NULL),(7,NULL,'FormTitle1','2019-12-07'),(8,NULL,'FormTitle2','2019-12-07');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booksatlibraries`
--

DROP TABLE IF EXISTS `booksatlibraries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `booksatlibraries` (
  `BookId` int(11) NOT NULL,
  `LibraryId` int(11) NOT NULL,
  `Quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`BookId`,`LibraryId`),
  KEY `FK_Libraries_BooksAtLibraries` (`LibraryId`),
  CONSTRAINT `FK_Books_BooksAtLibraries` FOREIGN KEY (`BookId`) REFERENCES `books` (`Id`),
  CONSTRAINT `FK_Libraries_BooksAtLibraries` FOREIGN KEY (`LibraryId`) REFERENCES `libraries` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booksatlibraries`
--

LOCK TABLES `booksatlibraries` WRITE;
/*!40000 ALTER TABLE `booksatlibraries` DISABLE KEYS */;
INSERT INTO `booksatlibraries` VALUES (1,1,20),(1,2,20),(2,1,15);
/*!40000 ALTER TABLE `booksatlibraries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booksbyauthor`
--

DROP TABLE IF EXISTS `booksbyauthor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `booksbyauthor` (
  `BookId` int(11) NOT NULL,
  `AuthorId` int(11) NOT NULL,
  PRIMARY KEY (`BookId`,`AuthorId`),
  KEY `FK_BooksByAuthor_Authors_Id` (`AuthorId`),
  CONSTRAINT `FK_BooksByAuthor_Authors_Id` FOREIGN KEY (`AuthorId`) REFERENCES `authors` (`Id`),
  CONSTRAINT `FK_BooksByAuthor_Books_Id` FOREIGN KEY (`BookId`) REFERENCES `books` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booksbyauthor`
--

LOCK TABLES `booksbyauthor` WRITE;
/*!40000 ALTER TABLE `booksbyauthor` DISABLE KEYS */;
INSERT INTO `booksbyauthor` VALUES (1,1),(4,1),(8,1),(5,2),(6,2),(1,3),(5,3),(8,3),(2,4),(6,4),(3,5),(8,5);
/*!40000 ALTER TABLE `booksbyauthor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booksbycategory`
--

DROP TABLE IF EXISTS `booksbycategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `booksbycategory` (
  `CategoryId` int(11) NOT NULL,
  `BookId` int(11) NOT NULL,
  PRIMARY KEY (`CategoryId`,`BookId`),
  KEY `FK_Books_Id` (`BookId`),
  CONSTRAINT `FK_Books_Id` FOREIGN KEY (`BookId`) REFERENCES `books` (`Id`),
  CONSTRAINT `FK_Categories_Id` FOREIGN KEY (`CategoryId`) REFERENCES `categories` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booksbycategory`
--

LOCK TABLES `booksbycategory` WRITE;
/*!40000 ALTER TABLE `booksbycategory` DISABLE KEYS */;
INSERT INTO `booksbycategory` VALUES (1,1),(2,1),(3,2),(4,2),(2,3),(3,3),(3,5),(1,8),(3,8),(5,8);
/*!40000 ALTER TABLE `booksbycategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `categories` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(80) DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Romance',0),(2,'Fantasy',0),(3,'Mystery',1),(4,'PoetryTEST',0),(5,'Fairytale',0);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `libraries`
--

DROP TABLE IF EXISTS `libraries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `libraries` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Address` varchar(500) DEFAULT NULL,
  `Details` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `libraries`
--

LOCK TABLES `libraries` WRITE;
/*!40000 ALTER TABLE `libraries` DISABLE KEYS */;
INSERT INTO `libraries` VALUES (1,'Library 1','Testinė g. 2',NULL),(2,'Library 2','Testinė g. 2',NULL);
/*!40000 ALTER TABLE `libraries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `members` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Gender` int(11) DEFAULT NULL,
  `LoginName` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Firstname` varchar(255) DEFAULT NULL,
  `Lastname` varchar(255) DEFAULT NULL,
  `PhoneNumber` varchar(25) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `AdditionalInfo` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'library'
--

--
-- Dumping routines for database 'library'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-07 15:09:53
