﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    [Table("books")]
    public class Book
    {
        [Key]
        public int Id { get; private set; }

        public string Isbn { get; private set; }

        public string Title { get; private set; }

        public DateTime? DateOfPublish { get; private set; }

        private Book() { }

        public Book(string isbn, string title, DateTime? dateOfPublish)
        {
            Isbn = isbn;
            Title = title;
            DateOfPublish = dateOfPublish;
        }

    }
}
